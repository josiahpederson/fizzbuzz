<img src="images/fizz.jpeg" alt="fizz"/>

# Fizzbuzz
Check out the deployed site [here](https://josiahpederson.gitlab.io/fizzbuzz/)! (Allow a few moments for the Heroku servers to spin up.)
<br/><br/>
This simple application is designed to identify numbers divisible by 3 and/or 5. "Fizz" is displayed if the number in question is divisible by 3. "Buzz" is displayed if the number is divisible by 5. "FizzBuzz" is displayed if the number is divisible by both. Additionally, the top three most popular queries are displayed. Enjoy!

## Getting started
Follow these simple steps to get the code up and running in your own development enviroment. You can also view the site live [here](https://josiahpederson.gitlab.io/fizzbuzz/).
1. This repo uses Docker. Install and run the [Docker Desktop](https://www.docker.com/products/docker-desktop/) program before continuing.
2. Clone the repo and `cd` into the directory.
3. Run `./initialize` from the terminal. Your browser will automatically open to `localhost:3000` when the script completes. (Probably 3 minutes)

## Documentation
* [API information](docs/api.md)
* [Database schema information](docs/schema.md)
* [GUI wireframes](docs/wireframes/)

## Tech stack
* Docker compose (frontend, backend, and database containers)
* React
* Flask
* Postgres
* Bash script for initialization and testing
* Gitlab CI Pipeline
* Heroku servers

## FAQ's
What was your reasoning behind the tech stack used for this challenge?
* This is a very lightweight application that should do its one job efficiently. I chose the above tech stack because Flask and React are quick to get up and running and because Docker and Gitlab make collaborative development and continuous deployment convenient. Also, Postgres databases are structured neatly for developers to quickly understand. A NoSQL database such as MongoDB would add some additional read speed, but it would have taken longer for me to implement.

What piece of this challenge did you find the most challenging and why?
* I've not used Flask with a database before, and I needed to dig through the Flask documentation to get the information I needed to get things off the ground. I will often use Django for my projects, but all the Django baggage would slow down this simple application down. This was the most challenging and time consuming aspect of the project.

How would you deploy this application for external users to use?
* I did [deploy a version](https://josiahpederson.gitlab.io/fizzbuzz/) to Gitlab Pages (frontend) & Heroku (backend and database). A deployment Dockerfile was used for the Flask framework and a node.js Docker image was used for the frontend. I used a gitlab-ci pipeline to allow for easy CI/CD. If I had more time, I would have deployed the backend to a more reliable service such as AWS or GCP. However, for a simple application, Heroku seemed a good fit.

After deploying your code, it seems to have gone viral as the most awesome app ever created in the history of time itself. How would you handle scaling and support your FizzBuzz app?
* I'd be amazed for a bit. Thankfully, Docker eases this challenge immensely. I would examine where traffic was most heavily originating from and horizontally scale using additional containerized backend instances and associated databases in those areas. Heroku would be replaced with AWS/GCP/Azure. A queue would handle which servers to send requests to. It might be a good time to switch to a document database like MongoDB or Redis, because they read faster and can handle some of the queueing. To manage database synchronization, I would instantiate some sort of pub/sub or polling between servers to keep the top 3 queried numbers relatively in sync. (Also, I would start running ads to pay for all this.)
