# API documentation

This page gives details on the hitherto inconceivable API endpoint.

### Submit a number to see if it is divisible by 3 or 5

When you submit a number, it is analyzed and the fizzbuzz result is returned. Additionally, the top three most common numbers are sent. Keeping these in the same API call reduces the number of API calls by half and speeds up the results of a query.

* **Method**: `POST`
* **Path**: /fizzbuzz

Input:

```json
{
  "number": integer,
}
```

Output:

```json
[
	{
		"fizzbuzz": string,
		"most_common": array,
	},
]
```
