# Schema Documentation
This page lays out the immensely complex schema of the database.

## num_frequency Table
This table represents the number of times each number has been queried.

| Name | Type | Unique | Optional |
|-|-|-|-|
| number | INT PRIMARY KEY | yes | no |
| frequency | INT | no | no |
