import React from "react";

const IsFizzbuzzTable = ({mostCommon}) => {
  return (
    <div className="p-3 mt-3">
      <h5>Most common queries</h5>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Number</th>
            <th>Frequency</th>
          </tr>
        </thead>
        <tbody>
          {mostCommon.map(({number, frequency}) => {
            return (
              frequency ?
                <tr key={number}>
                  <td>{number}</td>
                  <td>{frequency}</td>
                </tr>
              :
                null
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default IsFizzbuzzTable;
