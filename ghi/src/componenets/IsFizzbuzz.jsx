import React, { useState } from "react";
import IsFizzbuzzForm from "./IsFizzbuzzForm";
import IsFizzbuzzTable from "./IsFizzbuzzTable";

function IsFizzbuzz() {
  const [number, setNumber] = useState(0);
  const [result, setResult] = useState("")
  const [mostCommon, setMostCommon] = useState([])

  return (
    <div className="rounded shadow p-5 container mt-5 col-4">
        <div>A number divisible by 3 results in "Fizz"</div>
        <div>A number divisible by 5 results in "Buzz"</div>
        <div>A number divisible by both results in "FizzBuzz"</div>
        <div className="mt-2" style={{display: 'flex',
          justifyContent:'center', alignItems:'center'}}
        >
        <h1>{result ? result : "???"}</h1>
        </div>
        <IsFizzbuzzForm
          setResult={setResult} result={result}
          setMostCommon={setMostCommon} setNumber={setNumber}
          number={number}
        />
        <IsFizzbuzzTable mostCommon={mostCommon} />
    </div>
  );
}

export default IsFizzbuzz;
