import React from "react";
const BASE_URL = process.env.REACT_APP_FIZZBUZZ;

const IsFizzbuzzForm = ({setResult, result, setMostCommon, setNumber, number}) => {

  async function onSubmit(event) {
    event.preventDefault();
    const url = `${BASE_URL}/fizzbuzz`;

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({"number": number}),
      headers: {
      "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      setResult(() => data.fizzbuzz);
      setMostCommon(() => data.most_common);
    } else {
      console.log("Error:", response);
    }
  }

  const updateNumber = (event) => {
    setNumber(() => event.target.value);
  }

    return (
      <div className="card p-3 mt-3">
        <p>{result ? "Enter another number!" : "Enter a number below"}</p>
        <form onSubmit={onSubmit}>
          <div className="form-floating mb-3">
            <input className="form-control"
              value={number}
              onChange={updateNumber}
              type="number" name="number"
              placeholder="Enter number"
              minLength={1}
              maxLength={100}
            />
            <label htmlFor="number">Number</label>
          </div>
          <button className="btn btn-success" type="submit">Submit</button>
        </form>
      </div>
    );
}

export default IsFizzbuzzForm;
