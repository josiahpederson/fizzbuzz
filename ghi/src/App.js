import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import IsFizzbuzz from './componenets/IsFizzbuzz';

function App() {

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <BrowserRouter basename={basename}>
      <Routes>
        <Route index element={<IsFizzbuzz />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
