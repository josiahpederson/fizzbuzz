import os
import psycopg2


DB_URL = os.environ.get("DATABASE_URL")

print("Attempting database initialization.")
conn = psycopg2.connect(DB_URL)
cur = conn.cursor()
cur.execute("DROP TABLE IF EXISTS num_frequency;")
cur.execute(
    "CREATE TABLE num_frequency (number integer PRIMARY KEY,"
    "frequency integer NOT NULL);"
)
conn.commit()
cur.close()
conn.close()
print("Database table created.")
