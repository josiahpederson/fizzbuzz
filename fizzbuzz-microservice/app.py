from flask import Flask, request, Response
from flask_cors import cross_origin
import json
import os
import sys

parent_module = sys.modules[".".join(__name__.split(".")[:-1]) or "__main__"]
if __name__ == "__main__" or parent_module.__name__ == "__main__":
    from calculations import Fizzbuzz
    from db_queries import dbQueries
else:
    from .calculations import Fizzbuzz
    from .db_queries import dbQueries


app = Flask(__name__)
origins = ["http://localhost:3000", os.environ.get("CORS_HOST", None)]


@app.route("/fizzbuzz", methods=["POST"])
@cross_origin(origin=origins)
def divisible(fb: Fizzbuzz = Fizzbuzz(), db: dbQueries = dbQueries()) -> Response:
    data = request.get_json()
    number = int(data.get("number"))
    db.increment(number)
    fb_string = fb.is_fizzbuzz(number)
    most_common = db.most_common()
    return Response(json.dumps({"fizzbuzz": fb_string, "most_common": most_common}))


if __name__ == "__main__":
    app.run(debug=True)
