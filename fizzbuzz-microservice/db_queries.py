import psycopg2
import os


DB_URL = os.environ.get("DATABASE_URL")


class dbQueries:
    def connect_to_db(self):
        conn = psycopg2.connect(DB_URL)
        return conn

    def most_common(self) -> list:
        conn = self.connect_to_db()
        cur = conn.cursor()
        cur.execute("SELECT * FROM num_frequency " "ORDER BY frequency DESC LIMIT 3;")
        numbers = cur.fetchall()
        while len(numbers) < 3:
            numbers.append((None, None))
        cur.close()
        conn.close()
        return self.format_most_common(numbers)

    def get_frequency(self, number: int) -> list:
        conn = self.connect_to_db()
        cur = conn.cursor()
        cur.execute("SELECT * FROM num_frequency WHERE number=%s;", (str(number),))
        result = cur.fetchall()
        cur.close()
        conn.close()
        return result

    def add(self, number: int) -> tuple:
        conn = self.connect_to_db()
        cur = conn.cursor()
        cur.execute(
            "INSERT INTO num_frequency " "(number, frequency) VALUES (%s, %s);",
            (number, 0),
        )
        conn.commit()
        result = self.get_frequency(number)
        cur.close()
        conn.close()
        return result

    def increment(self, number: int) -> tuple:
        value = self.get_frequency(number)
        if not value:
            self.add(number)
        conn = self.connect_to_db()
        cur = conn.cursor()
        cur.execute(
            "UPDATE num_frequency " "SET frequency=frequency + 1 " "WHERE number=%s;",
            (number,),
        )
        conn.commit()
        result = self.get_frequency(number)
        cur.close()
        conn.close()
        return result

    def format_most_common(self, numbers: list) -> dict:
        formatted_nums = []
        for num, freq in numbers:
            dict_vals = {"number": num, "frequency": freq}
            formatted_nums.append(dict_vals)
        return formatted_nums
