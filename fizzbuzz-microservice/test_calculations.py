import sys

parent_module = sys.modules[".".join(__name__.split(".")[:-1]) or "__main__"]
if __name__ == "__main__" or parent_module.__name__ == "__main__":
    from calculations import Fizzbuzz
else:
    from .calculations import Fizzbuzz

fb = Fizzbuzz()


def test_fizz() -> None:
    assert fb.is_fizz(3) == True
    assert fb.is_fizz(4) == False


def test_buzz() -> None:
    assert fb.is_buzz(5) == True
    assert fb.is_buzz(6) == False


def test_fizzbuzz() -> None:
    assert fb.is_fizzbuzz(15) == "FizzBuzz"
    assert fb.is_fizzbuzz(10) == "Buzz"
    assert fb.is_fizzbuzz(9) == "Fizz"
