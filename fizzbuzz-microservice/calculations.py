class Fizzbuzz:
    def is_fizz(self, number: int) -> bool:
        return number % 3 == 0

    def is_buzz(self, number: int) -> bool:
        return number % 5 == 0

    def is_fizzbuzz(self, number: int) -> str:
        result = ""
        if self.is_fizz(number):
            result += "Fizz"
        if self.is_buzz(number):
            result += "Buzz"
        return result if result else "No Fizz or Buzz for you."
